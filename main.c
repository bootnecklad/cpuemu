/*CPUEmu. Copyright (C) 2015, John Tzonevrakis, licensed under the GNU GPL version 2.0:*/
/*Copyright (C) 2015  John Tzonevrakis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 */
#include <stdio.h>
#include <stdlib.h>
/*
 * CPU description: 
 *
 * Registers:
 *  Our CPU has two registers, X and Y. Each of them is capable of holding
 *  An integer (You can tweak the code by adding typedefs/etc/etc to make them
 *  hold a specific amount of data (eg. an 8-bit value), but this is out ofx
 *  the scope of this (educational) program. 
 * Memory:
 *  Our refference machine has 1024 bytes of memory. You can tweak this by
 *  adjusting the lenght of memory[].
 * Instruction set:
 *  Our instruction set is quite simplistic. It currently consists of 12
 *  instructions, which I will present below in table form:
 *  |  Mnemonic | Opcode |                             Purpose                                |    Example    |
 *  |    NOP    |  0x00  |                            Do nothing.                             |      NOP      |
 *  |    LDX    |  0x01  |                         Load a value to X.                         |    LDX #$0    |
 *  |    LDY    |  0x02  |                         Load a value to Y.                         |    LDY #$0    |
 *  |    JMP    |  0x03  |                         Jump to an address.                        |    JMP $000   |
 *  |    STX    |  0x04  |                        Store X to an address.                      |    STX $000   |
 *  |    STY    |  0x05  |                        Store Y to an address.                      |    STY $000   |
 *  |    SMX    |  0x06  |                        Store an address to X.                      |    SMX $000   |
 *  |    SMY    |  0x07  |                        Store an address to Y.                      |    SMY $000   |
 *  |    BEX    |  0x08  |        Branch to memory location 2, if location 1 equals X.        | BEX $000,$010 |
 *  |    BNX    |  0x09  |   Branch to memory location 2, if it location 2 does not equal X.  | BNX $000,$010 |
 *  |    ADX    |  0x0A  |                        Add a number to X.                          |    ADX #$10   |
 *  |    ADY    |  0x0B  |                        Add a number to Y.                          |    ADY #$10   |
 *  |    ORX    |  0x0C  |                        Bitwise OR with X.                          |    ORX #$10   |
 *  |    ORY    |  0x0D  |                        Bitwise OR with Y.                          |    ORY #$10   |
 *  |    ANX    |  0x0E  |                        Bitwise AND with X.                         |    ANX #$10   |
 *  |    ANY    |  0x0F  |                        Bitwise AND with Y.                         |    ANY #$10   |
 *  |    XOX    |  0x10  |                        Bitwise XOR with X.                         |    XOX #$10   |
 *  |    XOY    |  0x11  |                        Bitwise XOR with Y.                         |    XOY #$10   |
 *  |    NOX    |  0x12  |                         Bitwise NOT of X.                          |      NOX      |
 *  |    NOY    |  0x13  |                         Bitwise NOT of Y.                          |      NOY      |
 *  |    LIX    |  0x14  |                Load the value at the address in Y to X.            |      LIX      |
 *  |    LIY    |  0x15  |                Load the value at the address in X to Y.            |      LIY      |
 *  |    HLT    |  0xFF  |                            Halt CPU                                |      HLT      |
 * TODO:
 *  [P] Implement math operators. :
 *   Not all bitwise math operators are implemented. However, ADX and ADY *will* be implemented.
 *  [ ] Create an assembler.
 *  [X] Read program from a file.
 *  [ ] Create some form of text I/O.
 * Legend:
 *   [X] -- Done.
 *   [P] -- Partially implemented.
 *   [N] -- Won't be implemented.
 *   [ ] -- Will (most probably) be implemented.
 */

FILE *fp;
int X;
int Y;
int PC=0;
int memory[1024];
int halt;
int main();
void interpret(int val);

int main(int argc, char *argv[])
{
  if(argc != 2) 
    {
      fprintf(stderr, "Usage: cpu filename\n");
      exit(EXIT_FAILURE);
    } 
  /* Open the file which will be used to load memory contents. */
  fp = fopen(argv[1], "r");
  if(fp == NULL)
    {
      fprintf(stderr, "FATAL: Cannot open file for reading.\n");
      exit(EXIT_FAILURE);
    }

  int data;
  int addr=0;
  
  while(1)
    {
      data = fgetc(fp);
      
      if(feof(fp)) {
        break ;
      }
      memory[addr] = data;
      addr++;
    }
  
  fclose(fp);

  // "Walk through" the memory, interpreting whatever we find.
  halt = 0;
  addr = 0;
  while(!halt)
    {
      // DEBUG: Show the states of X, Y and PC
      printf("| X: %x | Y: %x | PC: %x |\n", X,Y,PC);
      // printf("Instruction: %d\n", memory[PC]);
      interpret(memory[PC]);
    }
  
  return 0;
}

void interpret(int val)
{
  /*Interpret the opcode passed as parameter.*/
  switch (val)
    {
    case 0x00:
      /* Do nothing (NOP) */
      PC++;
      break;
      
    case 0x01:
      /* Load to X (LDX) */
      X = memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x02:
      /* Load to Y (LDY) */
      Y = memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x03:
      /* Go to a memory location (JMP) */
      PC = memory[PC+1];
      break;
      
    case 0x04:
      /* Store X to specified memory location (STX) */
      memory[PC+1] = X;
      PC=PC+2;
      break;
      
    case 0x05:
      /* Store Y to specified memory location (STY) */
      memory[PC+1] = Y;
      PC=PC+2;
      break;
      
    case 0x06:
      /* Make X equal to a specified memory location (SMX) */
      X = memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x07:
      /* Make Y equal to a specified memory location (SMY) */
      Y = memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x08:
      /* Branch to a specific memory location, if the memory address given
       * as parameter equals X. (BEX) */
      if(X == memory[memory[PC+1]]) {
        PC = memory[PC+2];
      }
      else {
        PC=PC+2;
      }
      break;
      
    case 0x09:
      /* Branch to a specific memory location, if the memory address
       * given as parameter does *not* equal X. (BNX) */
      if(X != memory[memory[PC+1]]) {
        PC = memory[PC+2];
      }
      else {
        PC=PC+3;
      }
      break;
      
    case 0x0A:
      /* Add to X (ADX) */
      X = X + memory[PC+1];
      PC=PC+2;;
      break;
      
    case 0x0B:
      /* Add to Y (ADY) */
      Y = Y + memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x0C:
      /* Bitwise OR with X (ORX) */
      X = X | memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x0D:
      /* Bitwise OR with Y (ORY) */
      Y = Y | memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x0E:
      /* Bitwise AND with X (ANX) */
      X = X & memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x0F:
      /* Bitwise AND with Y (ANY) */
      Y = Y & memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x10:
      /* Bitwise XOR with X (XOX) */
      X = X ^ memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x11:
      /* Bitwise XOR with Y (XOY) */
      Y = Y ^ memory[PC+1];
      PC=PC+2;
      break;
      
    case 0x12:
      /* Bitwise NOT of X (NOX) */
      X = ~X;
      PC++;
      break;

    case 0x13:
      /* Bitwise NOT ox Y (NOY) */
      Y = ~ Y;
      PC++;
      break;

    case 0x14:
      /* Load the value at the address in Y to X. (LIX) */
      X = memory[Y];
      PC++;
      break;

    case 0x15:
      /* Load the value at the address in X to Y. (LIY) */
      Y = memory[X];
      PC++;
      break;
      
    case 0xFF:
      /* Halt CPU */
      halt = 1;
      printf("CPU Halted at 0x%d\n", PC);
      break;
    }
}
