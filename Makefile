CC=gcc
CFLAGS=-std=c99 -Wall -Wunused -Werror -Wextra -pedantic -lm

all: cpu
cpu: main.o 
	$(CC) $(CFLAGS) main.o -o cpu
clean:
	rm -rf cpu *.o
